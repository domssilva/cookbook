# cookbook

Collection of tiny programs that each demonstrate a particular programming concept. 
The Cookbook Method is the process of learning a programming language by building up a repository of small programs